<?php
@include 'utils/session.php';
$_SESSION['search-query'] = '';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/root.css" />
    <link rel="stylesheet" href="./css/header.css" />
    <link rel="stylesheet" href="./css/footer.css" />
    <link rel="stylesheet" href="./css/about.css" />
    <title>Cats | About</title>
  </head>
  <body>
    <?php 
    @include 'components/header.php';
    ?>

    <main class="main">
      <div class="main__logo">
        <img src="./imgs/logo.svg" alt="logo" />
      </div>
      <div class="main__description">
        <p class="small_font"> 
          "Коты" - это сайт о <span class="tooltip"><span class="tooltip__text small_font">Млекопитающее семейства кошачьих отряда хищных</span><span>котах</span></span>, которые ищут новый дом. Но эти коты такие
          же личности, как и каждый человек, и в каждом из них могут быть 
          <!-- <br /> -->
          недостатки, из-за которых от них могут отвернуться или бросить их. Для
          того, чтобы минимизировать переезды из одного дома в другой,
          <!-- <br /> -->
          на этом сайте показаны личности всех котов в виде их личных блогов!
          <!-- <br /> -->
          
        </p>
        <p class="small_font">
          Чтобы забрать котика, Вам необходимо всей душой полюбить его.
        </p>
      </div>
      <div class="main__comment-block">
        <p class="usual_font">
          Если у вас есть идеи, как сделать мир добрее к котам,<br />
          или как улучшить сайт, то заполните форму
        </p>
        <form action="#" method="get">
          <div class="form__user-info">
            <input class="small_font bordered" type="text" placeholder="Ваш email" />
            <input class="small_font bordered" type="text" placeholder="Ваше имя" />
          </div>
          <textarea  class="small_font bordered"
            cols="85"
            rows="6"
            placeholder="Ваше предложение"
          ></textarea>
          <button class="small_font bordered" type="submit">Отправить</button>
        </form>
      </div>
    </main>

    <?php 
    @include 'components/footer.php'
    ?>
  </body>

  <script type="text/javascript" src="./js/header.js"></script>
</html>
