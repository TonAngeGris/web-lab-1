<?php
@include 'utils/session.php';
@include 'utils/databaseConfig.php';
$_SESSION['search-query'] = '';

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/root.css" />
    <link rel="stylesheet" href="./css/header.css" />
    <link rel="stylesheet" href="./css/footer.css" />
    <link rel="stylesheet" href="./css/vasya-post-view.css" />
    <title>Cats | Vasya Post</title>
  </head>

  <body>
    <?php 
    @include 'components/header.php';
    ?>

    <?php 
    $post_id = $_SESSION['post-view-id'];

    $select = "SELECT ";
    $select .= "post_title, ";
    $select .= "post_description, ";
    $select .= "post_created_at, ";
    $select .= "(SELECT COUNT(post_id) FROM likes WHERE likes.post_id = posts.post_id) as like_count ";
    $select .= "FROM posts ";
    $select .= "WHERE post_id = {$post_id} ";
    $result = mysqli_query($connection, $select) or die(mysqli_error($connection));

    $post_data = mysqli_fetch_array($result);
    $post_created_at = date("d.m.Y", strtotime($post_data['post_created_at']));

    echo <<<LABEL
      <main class="main">
        <div class="post-title">
          <div class="post-title__text">{$post_data['post_title']}</div>
          <div class="post-title__date">{$post_created_at}</div>
        </div>
        <div class="description section">
          <div class="title">Описание</div>
          <div class="description__text">{$post_data['post_description']}</div>
        </div>
        <div class="gallery section">
          <div class="title">Фотографии</div>
          <ul class="gallery__content">
    LABEL;

    $select = "SELECT ";
    $select .= "photo_path ";
    $select .= "FROM photos ";
    $select .= "WHERE photos.post_id = {$post_id} ";
    $result = mysqli_query($connection, $select) or die(mysqli_error($connection));

    if (mysqli_num_rows($result) == 0) {
      echo 'Фотографии отсутствуют!';
    } else {
      while($photo = mysqli_fetch_array($result)) {
        echo "<li>";
        echo '<img class="img-to-zoom" src="'.$photo['photo_path'].'"/>';
        echo "</li>";
      }
    }

    echo <<<LABEL
          </ul>
        </div>
        <div class="video section">
          <div class="title">Видео</div>
    LABEL;

    $select = "SELECT video_path FROM videos WHERE videos.post_id = {$post_id}";
    $result = mysqli_query($connection, $select) or die(mysqli_error($connection));

    if (mysqli_num_rows($result) == 0) {
      echo 'Видеозапись отсутствует!';
    } else {
      $video = mysqli_fetch_array($result);

      echo '<video controls muted poster="./imgs/poster.png">';
      echo '<source src="'.$video['video_path'].'" type="video/mp4" />';
      echo '</video>';
    }

    echo <<<LABEL
        </div>
        <div class="tags section">
          <div class="title">Теги</div>
          <ul class="tag-list">
    LABEL;
            
    $select = "SELECT tag_name FROM tags WHERE tags.post_id = {$post_id}";
    $result = mysqli_query($connection, $select) or die(mysqli_error($connection));

    if (mysqli_num_rows($result) !== 0) {
      while($tag = mysqli_fetch_array($result)) {
        echo "<li>#".$tag['tag_name']."</li>";
      }
    }

    echo <<<LABEL
          </ul>
        </div>
        <div class="reactions section">
          <div class="title">Реакции</div>
          <div class="likes">
            <div class="counter">{$post_data['like_count']}</div>
            <div class="like__image">
              <img src="./imgs/like-reaction.svg" alt="like reaction" />
            </div>
          </div>
        </div>
      </main>
    LABEL;
    ?>

    <?php 
    @include 'components/footer.php'
    ?>   

    <div class="modal-container">
      <img
        class="zoomed-image"
        src="./imgs/vasya-zoomed.png"
        alt="vasya-photo-2"
      />
    </div>
  </body>

  <script type="text/javascript" src="./js/header.js"></script>
  <script type="text/javascript" src="./js/vasya-post-view.js"></script>
</html>
