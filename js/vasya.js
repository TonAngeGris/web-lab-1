const hamburgerBtn = document.querySelector('.hamburger');
const hamburgerMenu = document.querySelector('.hamburger-menu');
const hamburgerContentMenu = document.querySelector('.hamburger-menu .content_menu'); 
const hamburgerImg = document.getElementById('hamburger-img');
// const rightArrow = document.getElementById('right-arrow');

function hamburgerBtnHandler() {
  hamburgerMenu.style.display = 'flex';
  hamburgerContentMenu.style.display = 'flex';
}

function hideHamburgerMenu(event) {
  if ((event.target !== hamburgerImg && !event.target.closest('.hamburger-menu'))) {
    hamburgerMenu.style.display = 'none';
    hamburgerContentMenu.style.display = 'none';
  }
}

// function arrowHandler(event) {
//   console.log(event.target);
//   const action = "<?php increase_cur_page(); ?>";
//   document.write(action);
// }

hamburgerBtn.addEventListener('click', hamburgerBtnHandler);
document.addEventListener('click', hideHamburgerMenu);
// rightArrow.addEventListener('click', arrowHandler);
