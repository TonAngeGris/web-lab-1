const modalButtons = [...document.querySelectorAll('.modal-container__button')];
const checkboxes = [...document.querySelectorAll('input[type=checkbox]')];
const modalContainer = document.querySelector('.modal-container');
const likeButton = document.querySelector('.like-button__content');
const likeImage = document.getElementById('like-image');
const previousCarouselButton = document.getElementById('previous-button');
const nextCarouselButton = document.getElementById('next-button');
const carouselImage = document.getElementById('carousel-image');

function carouselImageHandler(event) {
  if (event.target === nextCarouselButton) {
    const imageNumber = Number(carouselImage.src.split('-').at(-1).split('.')[0]);
    carouselImage.src = carouselImage.src.replace(
      `-${imageNumber}.png`,
      `-${imageNumber + 1}.png`
    );

    event.target.parentElement.classList.add('carousel__button_inactive');
    previousCarouselButton.parentElement.classList.remove('carousel__button_inactive');
  } else {
    const imageNumber = Number(carouselImage.src.split('-').at(-1).split('.')[0]);
    carouselImage.src = carouselImage.src.replace(
      `-${imageNumber}.png`,
      `-${imageNumber - 1}.png`
    );

    event.target.parentElement.classList.add('carousel__button_inactive');
    nextCarouselButton.parentElement.classList.remove('carousel__button_inactive');
  }
}

function likeHandler(event) {
  if (event.target.src.includes('heart-gray')) {
    event.target.src = likeImage.src.replace('heart-gray', 'heart-red');
  } else {
    event.target.src = likeImage.src.replace('heart-red', 'heart-gray');
  }
}

checkboxes.forEach((elem) => {
  elem.addEventListener('click', () => {
    modalContainer.classList.add('modal-container_show');
  });
});

modalButtons.forEach((elem) => {
  elem.addEventListener('click', () => {
    modalContainer.classList.remove('modal-container_show');
  });
});

previousCarouselButton.addEventListener('click', carouselImageHandler);
nextCarouselButton.addEventListener('click', carouselImageHandler);
likeButton.addEventListener('click', likeHandler);
