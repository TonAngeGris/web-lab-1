const headerSelect = document.querySelector('.header__select');

function headerSelectHandler(event) {
  window.location.href = `${window.location.origin}/${event.target.value}.php`
}

headerSelect.addEventListener('change', headerSelectHandler);
