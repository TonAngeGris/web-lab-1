const imagesToZoom = document.getElementsByClassName('img-to-zoom');
const modalContainer = document.querySelector('.modal-container');
const zoomedImage = document.querySelector('.zoomed-image');
console.log([...imagesToZoom]);

function zoomImage(event) {
  console.log(event.target);
  zoomedImage.src = event.target.src;
  modalContainer.classList.add('modal-container_show');
}

function unzoomImage(event) {
  if (
    event.target.classList.contains('modal-container_show') &&
    !event.target.closest('.zoomed-image')
  ) {
    modalContainer.classList.remove('modal-container_show');
  }
}

[...imagesToZoom].forEach((elem) => {
  console.log('lol');
  elem.addEventListener('click', zoomImage);
});

// zoomedImage.addEventListener('click', zoomImage);
document.addEventListener('click', unzoomImage);
