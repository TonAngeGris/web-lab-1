<?php
@include 'utils/session.php';
@include 'utils/databaseConfig.php';

if(!isset($_SESSION['current-page'])) {
  $_SESSION['current-page'] = 1;
}

$cur_page = $_SESSION['current-page'];
$page_amount = 0;
$post_amount_on_page = 4;

$search_query = '';
if(!isset($_SESSION['search-query'])) {
  $_SESSION['search-query'] = '';
}

if(isset($_GET['search'])) {
  $_SESSION['search-query'] = $_GET['search'];
}
$search_query = $_SESSION['search-query'];

$select = "SELECT COUNT(post_id) FROM posts WHERE post_title LIKE '%".$search_query."%' OR post_description LIKE '%".$search_query."%'";
$result = mysqli_query($connection, $select) or die(mysqli_error($connection));
$page_amount = ceil(mysqli_fetch_array($result)[0] / $post_amount_on_page);

// If right arrow clicked
if(isset($_GET['right-arrow'])) {
  if($_SESSION['current-page'] < $page_amount) {
    $_SESSION['current-page']++;
  }

  header('location:vasya.php');
}

// If left arrow clicked
if(isset($_GET['left-arrow'])) {
  if($_SESSION['current-page'] > 1) {
    $_SESSION['current-page']--;
  }

  header('location:vasya.php');
}

// Go to each post page
$select = "SELECT post_id FROM posts";
$result = mysqli_query($connection, $select) or die(mysqli_error($connection));
$post_amount = mysqli_num_rows($result);

for($i = 1; $i <= $post_amount; $i++) {
  if(isset($_GET["view-post-".$i.""])) {
    $_SESSION['post-view-id'] = $i;
    header('location:vasya-post-view.php');
    break; 
  }
}

// Search by tags
unset($_SESSION['selected-tags']);

$select = "SELECT DISTINCT tag_name, tag_id FROM tags";
$result = mysqli_query($connection, $select) or die(mysqli_error($connection));
$total_tag_amount = mysqli_num_rows($result);

for ($i = 1; $i <= $total_tag_amount; $i++) {
  if(isset($_GET["tag-id-".$i.""])) {
    $select = "SELECT tag_name FROM tags WHERE tag_id = ".$i."";
    $result = mysqli_query($connection, $select) or die(mysqli_error($connection));
    $chosen_tag = mysqli_fetch_array($result);

    $_SESSION['selected-tags'][] = $chosen_tag['tag_name'];
  }
} 

if(isset($_GET['search-by-tags']) && isset($_SESSION['selected-tags'][0])) {
  $select = "SELECT ";
  $select .= "COUNT(posts.post_id) ";
  $select .= "FROM posts ";
  $select .= "LEFT JOIN tags ON tags.post_id = posts.post_id ";
  $select .= "WHERE post_title LIKE '%".$search_query."%' OR post_description LIKE '%".$search_query."%' ";
  $select .= "GROUP BY posts.post_id ";
  if(isset($_SESSION['selected-tags']) && count($_SESSION['selected-tags']) > 0) {
    $select .= "HAVING ";
    $select .= "GROUP_CONCAT(tags.tag_name) LIKE '%".$_SESSION['selected-tags'][0]."%' ";

    for($i = 1; $i < count($_SESSION['selected-tags']); $i++) {
      $select .= "AND GROUP_CONCAT(tags.tag_name) LIKE '%".$_SESSION['selected-tags'][$i]."%' ";
    } 
  }
  $result = mysqli_query($connection, $select) or die(mysqli_error($connection));

  // $page_amount = ceil(mysqli_fetch_array($result)[0] / $post_amount_on_page);
  $fetched_data = mysqli_fetch_array($result);
  if($fetched_data) {
    $page_amount = ceil($fetched_data[0] / $post_amount_on_page);
  } else {
    $page_amount = 0;
  }
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/root.css" />
    <link rel="stylesheet" href="./css/header.css" />
    <link rel="stylesheet" href="./css/footer.css" />
    <link rel="stylesheet" href="./css/vasya.css" />
    <title>Cats | Vasya</title>
  </head>
  <body>
    <?php
    @include 'components/header.php';
    ?>

    <main class="main">
      <div class="arrow">
        <a href="#header">
          <img src="./imgs/arrow.svg" alt="logo" />
        </a>
      </div>

      <button class="hamburger">
        <img id="hamburger-img" src="./imgs/hamburger.svg" alt="hamburger" />
      </button>

      <!-- Hamburger menu (adaptive layout for the left menu) -->
      <div class="menu hamburger-menu">
        <div class="content_menu">
          <p class="teg_name usual_font">Активные теги</p>
          <div class="choice_menu">
            <div class="teg_menu">
              <label class="switch">
                <input type="checkbox" />
                <span class="slider round"></span>
              </label>
              <p class="teg_name small_font">Спит</p>
            </div>
            <div class="teg_menu">
              <label class="switch">
                <input type="checkbox" />
                <span class="slider round"></span>
              </label>
              <p class="teg_name small_font">Ест</p>
            </div>
            <div class="teg_menu">
              <label class="switch">
                <input type="checkbox" />
                <span class="slider round"></span>
              </label>
              <p class="teg_name small_font">Орет</p>
            </div>
          </div>
          <button class="cat__button bordered">
            <p class="small_font">Искать</p>
          </button>
        </div>
      </div>

      <!-- Left menu -->
      <div class="content_main">
        <div class="menu">
          <div class="content_menu">
            <p class="teg_name usual_font">Активные теги</p>
            <form method="GET">
              <div class="choice_menu">
                <?php 
                
                $select = "SELECT DISTINCT tag_name, tag_id FROM tags";
                $result = mysqli_query($connection, $select) or die(mysqli_error($connection));

                while ($tag = mysqli_fetch_array($result)) {
                  echo '<div class="teg_menu">';
                  echo '<label class="switch">';
                  echo '<input type="checkbox" name="tag-id-'.$tag['tag_id'].'" />';
                  echo '<span class="slider round"></span>';
                  echo '</label>';
                  echo '<p class="teg_name small_font">'.$tag['tag_name'].'</p>';
                  echo '</div>';
                }
                ?>
              </div>
            
              <button name="search-by-tags" class="cat__button small_font bordered">Искать</button>
            </form>
          </div>
        </div>

        <div class="content">
          <div class="content_top">
            <div class="content_page">
              <form method="GET">
                <button name="left-arrow">
                  <?php 
                  if ($cur_page == 1 && $page_amount > 0) {
                    echo "<img class=\"content_page active\" src=\"imgs/pages_left_non.svg\"/>";
                  } else if ($page_amount > 0) {
                    echo "<img class=\"content_page active\" src=\"imgs/pages_left_act.svg\"/>";
                  }
                  ?>
                </button>
              </form>

              <?php 
              for ($i = 1; $i <= $page_amount; $i++) {
                if ($i == $cur_page) {
                  echo "<p class=\"active small_font\">{$cur_page}</p>";
                  continue;
                }
                
                echo "<p class=\"non_active small_font\">{$i}</p>";
              }
              ?>

              <form method="GET">
                <button name="right-arrow">
                  <?php 
                  if ($cur_page == $page_amount && $page_amount > 0) {
                    echo "<img class=\"content_page active\" src=\"imgs/pages_right_non.svg\"/>";
                  } else if ($page_amount > 0) {
                    echo "<img class=\"content_page active\" src=\"imgs/pages_right_act.svg\"/>";
                  }
                  ?>
                </button>
              </form>
            </div>
            <div>
              <div class="search bordered">
                <img src="imgs/search.svg" /> 
                <form class="search_enter_box" method="GET">
                  <?php 
                  if($search_query != '') {
                    echo "<input type=\"text\" class=\"search_enter small_font\" name=\"search\" placeholder=\"$search_query\"/>";
                  } else {
                    echo "<input type=\"text\" class=\"search_enter small_font\" name=\"search\" placeholder=\"Поиск\"/>";
                  }
                  ?>
                </form>
              </div>
            </div>
          </div>

          <?php           
          $like_count = 0;
          $image_count = 0;
          $video_count = 0;
          $timestamp = "";

          $first_post_on_page = ($cur_page - 1) * $post_amount_on_page; // (2 - 1) * 4 = 4

          $select = "SELECT ";
          $select .= "posts.post_id, ";
          $select .= "post_title,";
          $select .= "post_description,";
          $select .= "(SELECT COUNT(post_id) FROM likes WHERE likes.post_id = posts.post_id) as like_count,";
          $select .= "(SELECT COUNT(post_id) FROM photos WHERE photos.post_id = posts.post_id) as photo_count,";
          $select .= "(SELECT COUNT(post_id) FROM videos WHERE videos.post_id = posts.post_id) as video_count,";
          $select .= "GROUP_CONCAT(tags.tag_name) as post_tags, ";
          $select .= "post_created_at ";
          $select .= "FROM posts ";
          $select .= "LEFT JOIN tags ON tags.post_id = posts.post_id ";
          $select .= "WHERE post_title LIKE '%".$search_query."%' OR post_description LIKE '%".$search_query."%' ";
          $select .= "GROUP BY posts.post_id ";
          if(isset($_SESSION['selected-tags']) && count($_SESSION['selected-tags']) > 0) {
            $select .= "HAVING ";
            $select .= "post_tags LIKE '%".$_SESSION['selected-tags'][0]."%' ";
            
            for($i = 1; $i < count($_SESSION['selected-tags']); $i++) {
              $select .= "AND post_tags LIKE '%".$_SESSION['selected-tags'][$i]."%' ";
            } 
          }
          $select .= "LIMIT {$first_post_on_page}, {$post_amount_on_page}";
          $result = mysqli_query($connection, $select) or die(mysqli_error($connection));

          if (mysqli_num_rows($result) == 0) {
            echo 'Ни одного поста не найдено!';
          } else {
            while($row = mysqli_fetch_array($result)) {
              $post_created_at = date("d.m.Y", strtotime($row['post_created_at']));

              echo <<<LABEL
                <div class="content_box">
                  <div class="content_box_name">
                    <p class="medium_font">{$row['post_title']}</p>
                    <div class="content_box_status">
                      <div class="content_box_status_element">
                        <p class="like_count small_font">{$row['like_count']}</p>
                        <img
                          class="content_box_vector"
                          src="imgs/like.svg"
                          alt="like img"
                        />
                      </div>
                      <div class="content_box_status_element">
                        <p class="photo_count small_font">{$row['photo_count']}</p>
                        <img
                          class="content_box_vector"
                          src="imgs/photo.svg"
                          alt="like img"
                        />
                      </div>
                      <div class="content_box_status_element">
                        <p class="video_count small_font">{$row['video_count']}</p>
                        <img
                          class="content_box_vector"
                          src="imgs/video.svg"
                          alt="like img"
                        />
                      </div>
                    </div>
                  </div>
                  <p class="small_font">
                    {$row['post_description']}
                  </p>
                  <div class="content_box_bottom">
                    <p class="small_font">{$post_created_at}</p>
                    <div class="content_box_but">
                      <form method="GET">                      
                        <button class="cat__button small_font bordered" name="view-post-{$row['post_id']}">
                          Смотреть подробнее
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              LABEL;
            }
          }
          ?>
        </div>
      </div>
    </main>

    <?php
    @include 'components/footer.php';
    ?>
  </body>

  <script type="text/javascript" src="./js/header.js"></script>
  <script type="text/javascript" src="./js/vasya.js"></script>
</html>
