<header class="header">
  <div class="header__logo">
    <a href="./main.php">
      <img src="./imgs/logo.svg" alt="logo" />
    </a>
  </div>

  <div class="header__button-group">
    <select class="header__select small_font">
      <option value="default" selected hidden>Коты</option>
      <option value="lexus">Лексус</option>
      <option value="vasya">Вася</option>
    </select>
    <a href="./about.php" class="header__button small_font">О сайте</a>
    <?php 
    if(!empty($_SESSION['user'])) {
      echo "<a href=\"../account.php\" class=\"header__button small_font\"><u>".$_SESSION['user']."</u></a>";
    } else {
      echo "<a href=\"../login.php\" class=\"header__button small_font\">Войти</a>";
    }
    ?>
  </div>
</header>
