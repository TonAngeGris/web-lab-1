<?php
@include 'utils/databaseConfig.php';
@include 'utils/session.php';
$_SESSION['search-query'] = '';

$login = $_SESSION['user'];

if(isset($_POST['change-login'])) {
  $new_login = $_POST['login'];

  $update = "UPDATE users SET user_name = '$new_login' WHERE user_name = '$login'";
  mysqli_query($connection, $update);

  $_SESSION['user'] = $new_login;

  header('location:account.php');
}

if(isset($_POST['change-password'])) {
  $new_password = $_POST['password'];

  $update = "UPDATE users SET user_password = '$new_password' WHERE user_name = '$login'";
  mysqli_query($connection, $update);
  header('location:account.php');
}

if(isset($_POST['logout'])) {
  session_unset();
  session_destroy();
  header('location:main.php');
}

if(isset($_POST['delete-account'])) {
  $delete = "DELETE FROM users WHERE user_name = '$login'";
  mysqli_query($connection, $delete);
  session_unset();
  session_destroy();
  header('location:main.php');
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/root.css" />
    <link rel="stylesheet" href="./css/header.css" />
    <link rel="stylesheet" href="./css/footer.css" />
    <link rel="stylesheet" href="./css/account.css" />
    <title>Cats | Account</title>
  </head>
  <body>
    <?php 
    @include 'components/header.php';
    ?>

    <main class="main">
      <form method="POST"> 
        <div class="login-changing">
          <?php 
          echo "<input type=\"text\" id=\"login\" name=\"login\" placeholder=\"$login\" pattern=\"[a-zA-Z0-9]{4,255}\" title=\"Логин может содержать только латинские буквы и цифры, а так же быть длиной от 4 до 255 символов!\"/>"
          ?>
          <button type="submit" name="change-login">Изменить имя аккаунта</button>
        </div>
        <div class="password-changing">
          <input 
            type="password" 
            id="password" 
            name="password" 
            placeholder="Пароль" 
            pattern="[a-zA-Z0-9]{8,255}" 
            title="Пароль может содержать только латинские буквы и цифры, а так же быть длиной от 8 до 255 символов!" 
          />
          <button type="submit" name="change-password">Изменить пароль</button>
        </div>
        <div class="button-group">
          <button type="submit" name="logout">Выйти из аккаунта</button>
          <button type="submit" name="delete-account">Удалить аккаунт</button>
        </div>
      </form>
    </main>

    <?php   
    @include 'components/footer.php';
    ?>
  </body>

  <script type="text/javascript" src="./js/account.js"></script>
</html>
