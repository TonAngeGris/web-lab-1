<?php 
@include 'utils/session.php';
@include 'utils/databaseConfig.php';
$_SESSION['search-query'] = '';

if(isset($_POST['submit-registration'])){  
  $login = $_POST['login'];
  $password = $_POST['password'];
  $repeated_password = $_POST['repeated-password'];
  $gender = '';
  if(isset($_POST['male-gender'])) {
    $gender = 'M';
  }
  if(isset($_POST['female-gender'])) {
    $gender = 'F';
  }

  $registration_errors = [];

  if($password != $repeated_password) {
    $registration_errors[] = '<div>Пароли не совпадают!</div>';
  };

  $select = "SELECT * FROM users WHERE user_name = '$login'";
  $result = mysqli_query($connection, $select);
  if(mysqli_num_rows($result) > 0) {
    $registration_errors[] = '<div>Данный пользователь уже существует!</div>';
  };

  if(count($registration_errors) == 0) {
    $insert = "INSERT INTO users (user_name, user_gender, user_password) VALUES ('$login', '$gender', '$password')";
    mysqli_query($connection, $insert);
    header('location:main.php');
  };
};

if(isset($_GET['submit-login'])) {
  $login_errors = [];

  $login = $_GET['login'];
  $password = $_GET['password'];

  $select = "SELECT user_id FROM users WHERE user_name = '$login' AND user_password = '$password'";
  $result = mysqli_query($connection, $select);
  if(mysqli_num_rows($result) == 0) {
    $login_errors[] = 'Пользователя с таким логином или паролем не существует!';
  } 

  if(count($login_errors) == 0) {
    $_SESSION['user'] = $login;
    header('location:main.php');
  }   
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/root.css" />
    <link rel="stylesheet" href="./css/header.css" />
    <link rel="stylesheet" href="./css/footer.css" />
    <link rel="stylesheet" href="./css/login.css" />
    <title>Cats | Login</title>
  </head>
  <body>
    <?php 
    @include 'components/header.php';
    ?>

    <main class="main">
      <div class="login">
        <div class="login__text large_font">Вход</div>
        <form action="" method="get" >
          <input class="bordered" type="text" name="login" placeholder="Логин" />
          <input class="bordered" type="password" name="password" placeholder="Пароль" />
          <button class="bordered" type="submit" name="submit-login">Войти</button>

          <?php 
            if(isset($login_errors)) {
              foreach ($login_errors as $error) {
                echo $error;
              }
            }
          ?> 

        </form>
      </div>

      <div class="picture">
        <div class="picture__content">
          <img
            src="./imgs/registration-picture.png"
            alt="registration-picture"
          />
        </div>
        <div class="picture__text medium_font">
          Чтобы стать частью<br />
          сайта вам нужно войти<br />
          или<br />
          зарегистрироваться
        </div>
      </div>

      <div class="registration">
        <div class="registration__text large_font">Регистрация</div>
        <form action="" method="POST"> 
          <input 
          class="bordered"
            type="text" 
            name="login" 
            id="login"
            placeholder="Логин" 
            required
            pattern="[a-zA-Z0-9]{4,255}"
            title="Логин может содержать только латинские буквы и цифры, а так же быть длиной от 4 до 255 символов!" 
          />
          <input 
          class="bordered"
            type="password" 
            name="password" 
            placeholder="Пароль" 
            required
            pattern="[a-zA-Z0-9]{8,255}"
            title="Пароль может содержать только латинские буквы и цифры, а так же быть длиной от 8 до 255 символов!" 
          />
          <input 
          class="bordered"
            type="password" 
            name="repeated-password" 
            placeholder="Повтор пароля" 
            required
            pattern="[a-zA-Z0-9]{8,255}"
            title="Пароль может содержать только латинские буквы и цифры, а так же быть длиной от 8 до 255 символов!" 
          />
          <div class="registration__radio-group">
            <div class="radio-group__title">ваш пол</div>
            <label for="male">Муж</label>
            <input type="radio" name="male-gender" id="male" />
            <label for="female">Жен</label>
            <input type="radio" name="female-gender" id="female" />
          </div>
          <button class="bordered" type="submit" name="submit-registration">Зарегистрироваться</button>

          <?php 
            if(isset($registration_errors)) {
              foreach ($registration_errors as $error) {
                echo $error;
              }
            }
          ?> 
           
        </form>
      </div>
    </main>

    <?php 
    @include 'components/footer.php'
    ?>
  </body>

  <script type="text/javascript" src="./js/header.js"></script>
  <script type="text/javascript" src="./js/login.js"></script>
</html>
