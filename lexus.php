<?php
@include 'utils/session.php';
$_SESSION['search-query'] = '';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/root.css" />
    <link rel="stylesheet" href="./css/header.css" />
    <link rel="stylesheet" href="./css/footer.css" />
    <link rel="stylesheet" href="./css/lexus.css" />
    <title>Cats | Lexus</title>
  </head>
  <body>
    <?php   
    @include 'components/header.php';
    ?> 

    <main class="main">
      <div class="main__header">мяу!</div>
      <div class="main__text">А где Лексус?</div>
    </main>

    <?php 
    @include 'components/footer.php'
    ?>
  </body>

  <script type="text/javascript" src="./js/header.js"></script>
</html>
