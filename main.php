<?php
@include 'utils/session.php';
$_SESSION['search-query'] = '';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/root.css" />
    <link rel="stylesheet" href="./css/header.css" />
    <link rel="stylesheet" href="./css/footer.css" />
    <link rel="stylesheet" href="./css/main.css" />
    <title>Cats | Homepage</title>
  </head>
  <body>
    <?php 
    @include 'components/header.php';
    ?>
     
    <main class="main">
      <div class="carousel">
        <button class="carousel__button carousel__button_inactive">
          <img
            src="./imgs/left-arrow.svg"
            alt="left-arrow"
            id="previous-button"
          />
        </button>
        <div class="carousel__content">
          <img
            src="./imgs/carousel-0.png"
            alt="carousel-image"
            id="carousel-image"
          />
        </div>
        <button class="carousel__button">
          <img
            src="./imgs/right-arrow.svg"
            alt="right-arrow"
            id="next-button"
          />
        </button>
      </div>
      <div class="like-button__wrapper">
        <button class="like-button__content">
          <img src="./imgs/heart-gray.svg" alt="like" id="like-image" />
        </button>
      </div>

      <ul class="cats">
        <li>
          <div class="cat__photo">
            <img src="./imgs/vasya-list.png" alt="vasya" />
          </div>
          <div class="cat__info">
            <div class="large_font">Вася</div>
            <div class="cat__description usual_font">
              Любит сладкий перчик и будить в 5 утра, чтобы его <br />
              покормили.
            </div>
            <div class="cat__controls">
              <div class="cat__checkbox">
                <div class="checkbox__title usual_font">Вася хороший мальчик?</div>
                <label class="checkbox__label">
                  <input type="checkbox" />
                  <span class="fake-checkbox bordered"></span>
                  <span class="checkox__text usual_font">Да</span>
                </label>
              </div>
              <button class="cat__button small_font bordered">
                <a href="./vasya.php ">Посмотреть на Васю</a>
              </button>
            </div>
          </div>
        </li>

        <li>
          <div class="cat__photo">
            <img src="./imgs/lexus-list.png" alt="lexus" />
          </div>
          <div class="cat__info">
            <div class="cat__title large_font">Лексус</div>
            <div class="cat__description usual_font">
              Соседский кот, который заходит к нам домой и орет <br />
              на каждого, не позволяя себя трогать. Только кормить и играть.
            </div>
            <div class="cat__controls">
              <div class="cat__checkbox">
                <div class="checkbox__title usual_font">Лексус хороший мальчик?</div>
                <label class="checkbox__label">
                  <input type="checkbox" />
                  <span class="fake-checkbox bordered"></span>
                  <span class="checkox__text usual_font">Да</span>
                </label>
              </div>
              <button class="cat__button small_font bordered">
                <a href="./lexus.php">Посмотреть на Лексуса</a>
              </button>
            </div>
          </div>
        </li>
      </ul>
    </main>

    <?php 
      @include 'components/footer.php'
    ?> 

    <div class="modal-container">
      <div class="modal-container__content">
        <div class="modal-container__text medium_font">
          Вы хотите забрать этого кота <br />
          <span class="medium_font">домой?</span>
        </div>
        <div class="modal-container__button-group ">
          <button class="modal-container__button usual_font bordered">Да</button>
          <button class="modal-container__button usual_font bordered">Нет</button>
        </div>
      </div>
    </div>
  </body>

  <script type="text/javascript" src="./js/header.js"></script>
  <script type="text/javascript" src="./js/main.js"></script>
</html>
